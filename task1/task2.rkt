#lang racket
(require racket/gui/base)
(require table-panel)




;------------------------------------------------------------------------
; Creating and placing widgets

(define converter (new frame% [label "Temperature converter"]))

(define vertical-panel (new vertical-panel%
                            [parent converter]
                            [spacing 10]
                            [horiz-margin 10]
                            [vert-margin 20]
                            [alignment '(right center)]))

(define table-panel (instantiate table-panel%
                        [vertical-panel]
                        [dimensions '(2 2)]
                        [alignment '(left center)]
                        [row-stretchability #t]))

(define F-label (new message%
                     [parent table-panel]
                     [label "Fahrenheit:"]))

(define F-input (new text-field%
                     [parent table-panel]
                     [label #f]
                     [callback (λ (tf e) (convert-f))]))

(define C-label (new message%
                     [parent table-panel]
                     [label "Celsius:"]))

(define C-output (new text-field%
                     [parent table-panel]
                     [label #f]
                     [enabled #f]))

; Calculating tempurature in Celsius

(define (F->C str)
  (real->decimal-string (* (- (string->number str) 32) (/ 5 9.0)) 2))

(define (convert-f)
  (let ((str (string-normalize-spaces (send F-input get-value) " " "")))
    (if (not (string=? "" str))
        (if (not (equal? (string->number str) #f))
            (send C-output set-value (F->C str))
            (send C-output set-value "Mistake!"))
        (send C-output set-value ""))))

(send converter show #t)
(send F-input focus)
