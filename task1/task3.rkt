#lang racket
(require racket/gui/base)
(require table-panel)


; Calculating tempurature in Celsius and Fahrenheit

(define (convert-f)
  (let ((str (string-normalize-spaces (send input-field get-value) " " "")))
    (if (not (string=? "" str))
        (if (not (equal? (string->number str) #f))
            (begin
              (send F->C-output set-value (F->C str))
              (send C->F-output set-value (C->F str)))
            (begin
              (send F->C-output set-value "Mistake!")
              (send C->F-output set-value "Mistake!")))
        (begin
          (send F->C-output set-value "")
          (send C->F-output set-value "")))))


(define (F->C str)
  (real->decimal-string (* (- (string->number str) 32) (/ 5 9.0)) 2))

(define (C->F str)
  (real->decimal-string (+ (* (string->number str) (/ 9 5.0)) 32) 2))

;------------------------------------------------------------------------
; Creating and placing widgets

(define my-frame%
  (class frame%
    (define/override (on-subwindow-char frame event)
       (change event))
      (super-new)))

(define converter (new my-frame% [label "Temperature converter"]))

(define vertical-panel (new vertical-panel%
                            [parent converter]
                            [spacing 10]
                            [horiz-margin 20]
                            [vert-margin 20]
                            [alignment '(right center)]))

(define table-panel (new table-panel%
                         [parent vertical-panel]
                         [dimensions '(3 2)]
                         [alignment '(right center)]
                         [row-stretchability #t]))

(define input-label (new message%
                         [parent table-panel]
                         [label "Enter temperature:"]))

(define input-field (new text-field%
                         [parent table-panel]
                         [label #f]
                         [callback (λ (t e) (convert-f))]))

(define F->C-label (new message%
                        [parent table-panel]
                        [label "F -> C:"]))

(define F->C-output (new text-field%
                         [parent table-panel]
                         [label #f]
                         [enabled #t]))

(define C->F-label (new message%
                        [parent table-panel]
                        [label "C -> F:"]))

(define C->F-output (new text-field%
                         [parent table-panel]
                         [label #f]
                         [enabled #f]))

(send converter show #t)
(send input-field focus)

(define (change e)
  (send F->C-output focus))
